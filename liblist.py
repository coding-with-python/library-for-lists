# liblist.py

from liblog import log
import sys
import json

filename = 'errorcodes.json'

try:

  file = open(filename, 'r')
  errorcodes = json.load(file)        # Returns JSON object as a dictionary

except FileNotFoundError as err:

  log.error(f"{err.args[1]}: \'{filename}\'")
  sys.exit(err.args[0])

else:

  file.close()

#log.setLevel(10)


"""
 _____                                                                  _____ 
( ___ )                                                                ( ___ )
 |   |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|   | 
 |   |       __   ___  ___          __   ___  __  ___    __             |   | 
 |   |      /__` |__  |__  |__/    /__` |__  /  `  |  | /  \ |\ |       |   | 
 |   |      .__/ |___ |___ |  \    .__/ |___ \__,  |  | \__/ | \|       |   | 
 |___|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|___| 
(_____)                                                                (_____)
"""

def flatten(arg):
  """
  Function to flat a list.
  Addition: elements are casted as strings.

    Args:
      arg (list): nested list

    Returns:
      (list): not nested list. Each element is string type.
  """

  if not isinstance(arg, list | tuple): # if not a list or tuple
    return [str(arg)]
  return [x for sub in arg for x in flatten(sub)] # recurse and collect

def discard_inputs_for_seek(input1, input2):
  """
  Filter for the inputs of SEEK function. See 'errorcodes.json' for a detailed list of errors.
  
    Args:
      input1 (list): target list
      input2 (int | str): element to find

    Returns:
      (list): raised KO validations (if any)
  """

  errors = set()

  if not isinstance(input2, int | str): errors.add(errorcodes["10"])

  if input2 == "": errors.add(errorcodes["11"])
  
  if input1 == "": errors.add(errorcodes["4"])
  
  list(map(lambda x: log.error(x), errors))
  # EQUALS TO => for x in errors: log.error(x)

  return errors

def seek(input1, input2):
  """
  This function returns as a list the possitions (if any) of the element found on the proportioned list|tuple|string|integer. The search element must be single and the proportioned element can not contain lists as string (e.g. "[1, 2]")

    Args:
      input1 (list): target list
      input2 (int | string): element to find

    Returns:
      (list): new list with the positions of the element sought over the list.
  """

  log.debug("\"SEEK\" function")

  solution = []

  if not discard_inputs_for_seek(input1, input2):

    input1 = flatten(input1)
    input1 = "".join(list(input1)) # Concatenation into one string
    
    input2 = str(input2)

    log.debug(f"input1 = {input1}")
    log.debug(f"input2 = {input2}")

    j = 0
    
    for i in range (len(input1)):

      j = input1.find(input2, j)

      if ( j == -1 ):

        break

      else:
  
        log.debug(f"index = {j}")

        solution.append(j)

      j += 1

  log.debug(f"Result = {solution}")
  
  return solution





"""
 _____                                                                               _____ 
( ___ )                                                                             ( ___ )
 |   |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|   | 
 |   |       __   ___  __  ___  __   __          __   ___  __  ___    __             |   | 
 |   |      |  \ |__  /__`  |  |__) /  \ \ /    /__` |__  /  `  |  | /  \ |\ |       |   | 
 |   |      |__/ |___ .__/  |  |  \ \__/  |     .__/ |___ \__,  |  | \__/ | \|       |   | 
 |___|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|___| 
(_____)                                                                             (_____)
"""

def discard_inputs_for_destroy(input1, input2):
  """
  Filter for the inputs of DESTROY function. See 'errorcodes.json' for a detailed list of errors.

  
    Args:
      input1 (list): target list
      input2 (list): element/s to delete

    Returns:
      (list): raised KO validations (if any)
  """

  errors = set()

  if not isinstance(input2, list): errors.add(errorcodes["1"])

  if input2 == []: errors.add(errorcodes["2"])

  elif int(max(input2)) > len(input1): errors.add(errorcodes["6"])
  
  if not isinstance(input1, list): errors.add(errorcodes["3"])
  
  if input1 == []: errors.add(errorcodes["4"])

  if len(input2) > len(input1): errors.add(errorcodes["5"])

  if len(input2) > len(list(set(input2))): errors.add(errorcodes["7"])

  for x in input2:

    if not isinstance(x, int): errors.add(errorcodes["8"])

    elif x < 0: errors.add(errorcodes["9"])
  
  list(map(lambda x: log.error(x), errors))
  # EQUALS TO => for x in errors: log.error(x)

  return errors


def destroy(input1, input2):
  """
  This function deletes from a list the possitions given in another list.

    Args:
      input1 (list): target list
      input2 (list): element/s to delete
    
    Returns:
      (list): target list after treatment
  """

  log.info("\"DESTROY\" function")

  solution = input1

  if not discard_inputs_for_destroy(input1, input2):

    input2.sort(reverse=True)

    log.debug(f"input2 value = {input2}")

    for x in input2:

      try:
      
        del (solution[int(x)])

      except Exception as err:

        log.error(f"{err}".capitalize())

  log.debug(f"Result = {solution}")
  
  return solution