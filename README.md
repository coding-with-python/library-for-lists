
# Library for Lists

This is my first {+ Python+} project! It's pretty sure that there will be some functions/modules doing the same like right here, but just for practising programming it's worth for me.

The main idea is to delete an element from a list, but not just the first occurrence found, functions are prepared to be launched sequentially and will **search and/or delete the element over the full given list**.

> Important: due to the internal treatment on sets with Pyhton, some inputs (and tests) are not possible to perform and achieve correct response. Inputs with duplicated items, not sorted or nested are out of the scope of the proyect.

## Roadmap

1. [x] Seek function plus its tests

1. [ ] Destroy function plus its tests

1. [ ] Maybe redefine tests using Pytest.

## Usage/Examples

```python
from liblist import *

search_on = [1, 2, 1, 4, 1, 1, 4, 5]
element_to_search = 4

output = seek(search_on, element_to_search)
```

```python
output = [3,6]
```
---
```python
from liblist import *

search_on = [1, 2, 1, 4, 1, 1, 4, 5]
elements_to_delete = [3,6]

output = destroy(search_on, elements_to_delete)
```

```python
output = [1, 2, 1, 1, 1, 5]
```