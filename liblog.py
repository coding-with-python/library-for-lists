# liblog.py

import logging


class LogColorerFormatter(logging.Formatter):
    def __init__(self, fmt='[%(asctime)s] - %(module)s.%(lineno)d >> %(message)s', datefmt=None, style='%'):
        logging.Formatter.__init__(self, fmt, datefmt, style)

    def format(self, record):
        # Still apply the default formatter
        uncolored = logging.Formatter.format(self, record)

        reset = "\x1b[0m"

        color = "\x1b[37m"  # White (default, for DEBUG level)
        if(record.levelno >= logging.CRITICAL):
            color = "\x1b[41m\x1b[37m"  # Red bg, white text
        elif(record.levelno >= logging.ERROR):
            color = "\x1b[91m"  # Red text
        elif(record.levelno >= logging.WARNING):
            color = "\x1b[93m"  # Yellow text
        elif(record.levelno >= logging.INFO):
            color = "\x1b[92m"  # Green text
        elif(record.levelno >= logging.DEBUG):
            color = "\x1b[94m"  # Cyan text

        # If the logged string is already colored in places, allow that to override this
        colored = color + uncolored.replace(reset, reset + color) + reset
        return colored


formatter = LogColorerFormatter()
console_handler = logging.StreamHandler()
#console_handler.setLevel(logging.INFO)
console_handler.setFormatter(formatter)

log = logging.getLogger()
log.addHandler(console_handler)

# log.setLevel(0)  # show everything

# log.critical("This is a critical problem.")
# log.error("This is an error.")
# log.warning("This is a warning.")
# log.info("This is information.")
# log.debug("This is a debug message.")
