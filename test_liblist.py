# test_liblist.py
# NOTE - SUT: liblist.py

from liblog import log
import os
from liblist import *
from datetime import datetime

log.setLevel(20)

# SECTION: "SEEK"

# SECTION - Counters

seek_counters = {
  "TOTAL": 0,
  "OK": 0,
  "KO": 0,
  "SKIPPED": 0
}

# !SECTION


# SECTION - Encapsulation for assertions. Default values must be at the tail of the parameter list.
def test_seek(test_name, search_on, element_to_search, expected, activated = True):

  if not(activated):

    log.warning(f"{test_name} -> SKIPPED")
    seek_counters["SKIPPED"] += 1

  else:

    log.debug(f"Input = {search_on}")
    log.debug(f"Element = {element_to_search}")
    log.debug(f"Expected result = {expected}")

    output = seek(search_on, element_to_search)

    if output == expected:

      log.info(f"{test_name} -> OK")
      seek_counters["OK"] += 1
      return True

    else:

      log.error(f"{test_name} -> KO")
      log.error(f"-- Result  : {output}")
      log.error(f"-- Expected: {expected}")
      seek_counters["KO"] += 1
      return False

# !SECTION


# SECTION Test launcher
#         PROTOTYPE:
#           <string> test_name
#           <list | tuple | int | string | set> destiny_for_search
#           <int | string> element_to_search
#           <list> expected_result
#           <bool> (optional) launch_or_skip: True by default

# NOTE - Time effort calculation for tests
time_start = datetime.now()


# LIST
test_seek("Empty input list", [], 8, [])
test_seek("Search element as void list", ['1', 2, 3, 4], [], [])
test_seek("All paramenters are empty lists", [], [], [])
test_seek("List with empty search element as string", [1, 2, '3', '4', 2, 1], "", [])
test_seek("List with search element is a string", [1, '2', '3', 4, 2, 1], "2", [1, 4])
test_seek("List happy path", ['1', 2, 4, 1, '1', '4', 5], 4, [2, 5])
test_seek("List void result", ['1', 2, 1, '4', 1, '1', 4, 5], 44, [])
test_seek("Nested list as input", [1, 2, [1 , '4'], [1, [1, 4]], 1, 5], 4, [3, 6])
test_seek("Search element as a list with match", [1, [2, 3], 4], [2, 3], [])
test_seek("Search element as a list without a match", ['1', [2, 3], 4], '[2, 4]', [])
test_seek("List with multiple search elements", (1, 4, 1, '5'), [1, 5], [])


# TUPLES
test_seek("Empty input tuple", (), 2, [])
test_seek("Search element as void tuple", ('2', 4, 6, 8, 6, 4, '2'), (), [])
test_seek("All parameters are empty tuples", (), (), [])
test_seek("Tuple with empty search element as string", ('2', 4, 6, 8, 6, 4, '2'), "", [])
test_seek("Tuple with search element is a string", (2, 4, 6, 8, '6', 4, 2), "6", [2, 4])
test_seek("Tuple happy path", (2, 4, 6, 8, 6, '4', 2), 4, [1, 5])
test_seek("Tuple void result", ('2', 4, 6, 8, 6, 4, '2'), 7, [])
test_seek("Nested tuple as input", (2, (4, 6), (8, 6, '4'), 2), 4, [1, 5])
test_seek("Search element as a tuple with match", ('2', (4, 6), (8, 6, 4), '2'), (4, 6), [])
test_seek("Search element as a tuple without match", ('2', (4, 6), (8, 6, 4), '2'), (3, 4), [])
test_seek("Tuple with multiple search elements", (1, 4, 1, '5'), [1, 5], [])


# INTEGERS
test_seek("Integer as input, search element is void string", 121451145, "", [])
test_seek("Integer as input, search element is void list", 121451145, [], [])
test_seek("Integer as input, search as string element", 121451145, "4", [3, 7])
test_seek("Integer happy path, simple digit", 121451145, 5, [4,8])
test_seek("Integer happy path, multiple digits", 121451145, 14, [2, 6])
test_seek("Integer as input, pattern repeats", 4444254444415, 44, [0, 1, 2, 6, 7, 8, 9])
test_seek("Integer void result", 121451145, 78, [])
test_seek("Integer multiple search elements", 121451145, [2, 3], [])


# STRINGS
test_seek("Empty input string", "", 12, [])
test_seek("Empty inputs as strings", "", "", [])
test_seek("String as input, search element as void string", "121451145", "", [])
test_seek("String as input, search element as void list", "121451145", [], [])
test_seek("String as input, search as integer void result", "4444254 415", 98, [])
test_seek("String happy path, simple character", "121411 415", "1", [0, 2 , 4 , 5 , 8])
test_seek("String happy path, multiple characters", "121211 215", "12", [0, 2])
test_seek("String as input, pattern repeats", "44442544 44415", "44", [0, 1, 2, 6, 9, 10])
test_seek("String happy path, search has spaces", "121411 415", "1 ", [5])
test_seek("String happy path, search has spaces void result", "121411 415", "1  ", [])
test_seek("String as input, search as string void result", "4444254 415", "98", [])
test_seek("String as input, multiple search elements", "4444254 415", [0, 5], [])


# SETS: internal sort by hash in set elements makes impossible to perform some tests (added at the end of them).
test_seek("Empty input set", {}, 8, [])
test_seek("Search element as void set", ['1', 2, 3, 4], {}, [])
test_seek("Set with search element as void list", {'1', 2, 3, 4}, [], [])
test_seek("All paramenters are empty sets", {}, {}, [])
test_seek("Set with empty search element as string", {1, '2', 3, '4'}, "", [])
test_seek("Set void result", {'1', 2, 1, '4', 1, '1', 4, 5}, 44, [])
test_seek("Search element as a set with match", [1, [2, 3], 4], {2, 3}, [])
test_seek("Search element as a set without a match", ['1', [2, 3], 4], '{2, 4}', [])
test_seek("Set with multiple search elements", {1, 4, 1, 5}, [1, 5], [])
'''
test_seek("Set with search element is a string", {1, 2, '3', 4}, "2", [1])
test_seek("Set happy path", {'1', '2', 1, 4, 4, '1', '4', 5}, 4, [3, 6])
test_seek("Nested set as input", {'1', 2, {1 , '4'}, {1, {'1', 4}}, 1, 5}, 4, [3, 6])
'''


# !SECTION


# SECTION - Report

seek_counters["TOTAL"] = sum(seek_counters.values())

report = f"TESTSET for \"SEEK\" in {os.path.basename(__file__)} \n + Report: {seek_counters} \n + Time consumed: {datetime.now()-time_start}"

if ( seek_counters["KO"] > 0 ): log.error(report)

elif ( seek_counters["SKIPPED"] > 0 or seek_counters["TOTAL"] == 0): log.warning(report)

else: log.info(report)


# !SECTION

# !SECTION

# SECTION: "DESTROY"


# SECTION - Counters

destroy_counters = {
  "TOTAL": 0,
  "OK": 0,
  "KO": 0,
  "SKIPPED": 0
}

# !SECTION

# SECTION - Encapsulation for assertions. Default values must be at the tail of the parameter list.
def test_destroy(test_name, base_list, delete_list, expected, activated = True):

  if not(activated):

    log.warning(f"{test_name} -> SKIPPED")
    destroy_counters["SKIPPED"] += 1

  else:

    log.debug(f"Input = {base_list}")
    log.debug(f"Element = {delete_list}")
    log.debug(f"Expected result = {expected}")

    output = destroy(base_list, delete_list)

    if output == expected:

      log.info(f"{test_name} -> OK")
      destroy_counters["OK"] += 1
      return True

    else:

      log.error(f"{test_name} -> KO")
      log.error(f"-- Result  : {output}")
      log.error(f"-- Expected: {expected}")
      destroy_counters["KO"] += 1
      return False

# !SECTION

# SECTION Test launcher
#         PROTOTYPE:
#           <string> test_name
#           <list> input
#           <list> positions_to_delete
#           <list> expected_result
#           <bool> (optional) launch_or_skip: True by default

# NOTE - Time effort calculation for tests
time_start = datetime.now()

# LISTS
test_destroy("List happy path", ['1', 4, 1, '4', 5], [1, 3], ['1', 1, 5])
test_destroy("List with out of range delete element", ['1', 4], [4, 1], ['1', 4])
test_destroy("List with void input element", [], [2, 3], [])
test_destroy("List with void delete element", ['1', 4], [], ['1', 4])
test_destroy("List with too many elements to delete", ['1', 4], [0, 1, 1], ['1', 4])
test_destroy("List with duplicated elements to delete", ['1', 4], [0, 0], ['1', 4])
test_destroy("List with string elements to delete", ['1', 4], ['1', '0'], ['1', 4])
test_destroy("List with negative values to delete", ['1', 4], [0, -1], ['1', 4])
test_destroy("Nested list", ['1', 4, [3, 1]], [2], ['1', 4])

# SETS
test_destroy("Void delete element", {'1', 4, 1, '4', 5}, [2, 3], {'1', 4, 1, '4', 5})

# STRINGS
test_destroy("String delete element", ['1', 4, 1, '4', 5], "3", ['1', 4, 1, '4', 5])

# !SECTION

# SECTION - Report

destroy_counters["TOTAL"] = sum(destroy_counters.values())

report = f"TESTSET for \"DESTROY\" in {os.path.basename(__file__)} \n + Report: {destroy_counters} \n + Time consumed: {datetime.now()-time_start}"

if ( destroy_counters["KO"] > 0 ): log.error(report)

elif ( destroy_counters["SKIPPED"] > 0 or destroy_counters["TOTAL"] == 0): log.warning(report)

else: log.info(report)

# !SECTION

# !SECTION